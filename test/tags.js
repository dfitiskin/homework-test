'use strict';
const url = require('url');
const UrlPattern = require('url-pattern');
const request = require('request');
const cheerio = require('cheerio');
const expect = require('chai').expect;

let workUrl = 'http://codepen.io/ruuuubi/pen/jrELWV?editors=1000';
let parts = url.parse(workUrl);
var $, $section;

describe('Теги для разметки текста', () => {

  before(done => {
    if (parts.hostname !== 'codepen.io') done('Not codepen link');

    let pattern = new UrlPattern('/:user/pen/:code');
    let info = pattern.match(parts.pathname);
    if (!info) done('Bad codepen link');

    let target = `http://s.codepen.io/${info.user}/pen/${info.code}.html`;

    request(target, (error, response, html) => {
      if (error) done('Debug not opened');
      if (200 !== response.statusCode) process.exit(`Got status ${response.statusCode}`);

      $ = cheerio.load(html);
      done(false);
    });
  });

  describe('Задача №1.', () => {
    before (() => {
      $section = $('section').eq(0);
    })

    it('Есть заголовок второго уровня', () => {
      let $head = $section.children('h2');
      expect($head).to.have.length(1);
    });

    it('Текст заголовка совпадает', () => {
      let content = $section.children('h2').text();
      expect(content).to.equal('Что такое нейросеть');
    });

    it('Есть два параграфа', () => {
      let $par = $section.children('p');
      expect($par).to.have.length(2);
    });

    it('Во втором параграфе есть ссылка', () => {
      let $parA = $section.children('p').eq(1).children('a');
      expect($parA).to.have.length(1);
    });

    it('Адрес ссылки совпадает', () => {
      let $parAHref = $section.children('p').eq(1).children('a').attr('href');
      expect($parAHref).to.equal("https://meduza.io/shapito/2016/06/28/neyronnaya-oborona-robot-napisal-teksty-pesen-pod-egora-letova");
    });

    it('Текст ссылки совпадает', () => {
      let $parAText = $section.children('p').eq(1).children('a').text();

      expect($parAText).to.equal("научилась");
    });
  });

  describe('Задача №2.', () => {
    before (() => {
      $section = $('section').eq(1);
    })

    it('Есть заголовок второго уровня', () => {
      let $head = $section.children('h2');
      expect($head).to.have.length(1);
    });

    it('Текст заголовка совпадает', () => {
      let content = $section.children('h2').text();

      expect(content).to.equal("Дорога");
    });

    it('Есть два параграфа', () => {
      let $par = $section.children('p');
      expect($par).to.have.length(2);
    });

    it('Есть цитата', () => {
      let $block = $section.children('blockquote');
      expect($block).to.have.length(1);
    });

    it('В цитате есть параграф', () => {
      let $blockP = $section.children('blockquote').children('p');
      expect($blockP).to.have.length(1);
    });
  });

  describe('Задача №3.', () => {
    const h3 = [
      'Поселок Малки',
      'Налычевская долина',
      'Паратунка',
    ]

    const src = [
      'http://cdn02.mir.afisha.ru/imgs/2016/07/28/11/11818/32f4b0ce86c4b5c749c95ef413af81a657e43266.jpg',
      'http://cdn01.mir.afisha.ru/imgs/2016/07/28/12/11822/ed59309a072af4003bfe0e2757dc70bd7357000f.jpg',
      'http://cdn00.mir.afisha.ru/imgs/2016/07/28/10/11816/5db7233de2cfc092cb00d0b6314aaeb275dc592e.jpg'
    ]

    before (() => {
      $section = $('section').eq(2);
    })

    it('Есть заголовки третьего уровня', () => {
      let $heads = $section.children('h3');
      expect($heads).to.have.length(3);
    });

    it('Текст заголовков совпадает', () => {
      let content = $section.children('h3').map((i, node) => {
        return $(node).text();
      }).get().slice();

      expect(content).to.members(h3);
    });

    it('Есть три картинки', () => {
      let $imgs = $section.children('img');
      expect($imgs).to.have.length(3);
    });

    it('Alt картинок совпадают', () => {
      let $imgAlt = $section.children('img').map((i, node) => {
        return $(node).attr('alt');
      }).get().slice();

      expect($imgAlt).to.members(h3);
    });

    it('Адреса картинок совпадают', () => {
      let $imgSrc = $section.children('img').map((i, node) => {
        return $(node).attr('src');
      }).get().slice();

      expect($imgSrc).to.members(src);
    });

    it('Есть три абзаца', () => {
      let $pars = $section.children('p');
      expect($pars).to.have.length(3);
    });
  });
});
