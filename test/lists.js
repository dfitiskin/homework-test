'use strict';
const url = require('url');
const UrlPattern = require('url-pattern');
const request = require('request');
const cheerio = require('cheerio');
const expect = require('chai').expect;

let workUrl = 'http://codepen.io/dfitiskin/pen/KgKdJW?editors=1000';
let parts = url.parse(workUrl);
var $, $section;

describe('Списки и таблицы', () => {

  before(done => {
    if (parts.hostname !== 'codepen.io') done('Not codepen link');

    let pattern = new UrlPattern('/:user/pen/:code');
    let info = pattern.match(parts.pathname);
    if (!info) done('Bad codepen link');

    let target = `http://codepen.io/${info.user}/pen/${info.code}.html`;

    request(target, (error, response, html) => {
      if (error) done('Debug not opened');
      if (200 !== response.statusCode) process.exit(`Got status ${response.statusCode}`);

      $ = cheerio.load(html);
      done(false);
    });
  });

  describe('Задача №1. Маркерованный список', () => {
    const list = [
      'Мартин Лютер Кинг',
      'Роберт Оппенгеймер',
      'Альфред Хичкок',
      'Чарли Чаплин',
      'Коко Шанель'];

    before (() => {
      $section = $('section').eq(0);
    })

    it('Создан маркерованный список', () => {
      let $list = $section.find('ul');
      expect($list).to.have.length(1);
    });

    it('Список из 5 элементов', () => {
      let $li = $section.find('ul>li');
      expect($li).to.have.length(5);
    });

    it('Текст элементов совпадает', () => {
      let content = $section.find('ul>li').map((i, node) => {
        return $(node).text();
      }).get().slice();

      expect(content).to.members(list);
    });
  });

  describe('Задача №2. Нумерованный список', () => {
    const list = [
      'Билл Гейтс ($75 млрд)',
      'Амансио Ортега ($67 млрд)',
      'Уоррен Баффет ($60,8 млрд)',
      'Карлос Слим Элу ($50 млрд)',
      'Джефф Безос ($45,2 млрд)'];

    before (() => {
      $section = $('section').eq(1);
    })

    it('Создан нумерованный список', () => {
      let $list = $section.find('ol');
      expect($list).to.have.length(1);
    });

    it('Список из 5 элементов', () => {
      let $li = $section.find('ol>li');
      expect($li).to.have.length(5);
    });

    it('Текст элементов совпадает', () => {
      let content = $section.find('ol>li').map((i, node) => {
        return $(node).text();
      }).get().slice();

      expect(content).to.members(list);
    });

    it('Порядок элементов совпадает', () => {
      let content = $section.find('ol>li').each((i, node) => {
        expect($(node).text()).to.equal(list[i]);
      });
    });
  });

  describe('Задача №3. Стандартная таблица', () => {
    const td = [
      'Белоснежка',
      '12.00',
      '1 500 руб.',
      'Лунтик',
      '12.30',
      '4 800 руб.',
      'Маша и Медведь',
      '13.15',
      '8 400 руб.',
      'Бэтмен',
      '13.45',
      '5 200 руб.',
      'Джек Воробей',
      '14.30',
      '3 300 руб.',
      'Аватар',
      '14.50',
      '4 000 руб.',
      'Энгри Бердс',
      '15.20',
      '2 150 руб.',
    ];

    const final = [
      'итого',
      '29 350 руб.'
    ];

    before (() => {
      $section = $('section').eq(2);
    })

    it('Создана таблица', () => {
      let $table = $section.find('table');
      expect($table).to.have.length(1);
    });

    it('Создана шапка таблицы', () => {
      let $thead = $section.find('table>thead');
      expect($thead).to.have.length(1);
    });

    it('В шапке создана одна строка', () => {
      let $trh = $section.find('table>thead>tr');
      expect($trh).to.have.length(1);
    });

    it('В шапке создано три ячейки', () => {
      let $tdh = $section.find('table>thead>tr>td');
      expect($tdh).to.have.length(3);
    });

    it('Текст элементов совпадает', () => {
      let content = $section.find('table>tbody>tr>td').map((i, node) => {
        return $(node).text();
      }).get().slice();

      expect(content).to.members(td);
    });

    it('Создано тело таблицы', () => {
      let $tbody = $section.find('table>tbody');
      expect($tbody).to.have.length(1);
    });

    it('В теле таблицы 7 строк', () => {
      let $trb = $section.find('table>tbody>tr');
      expect($trb).to.have.length(7);
    });

    it('В каждой строке по три ячейки', () => {
      let $tdb = $section.find('table>tbody>tr>td');
      expect($tdb).to.have.length(21);
    });

    it('Создан подвал таблицы', () => {
      let $tfoot = $section.find('table>tfoot');
      expect($tfoot).to.have.length(1);
    });

    it('В подвале одна строка', () => {
      let $trf = $section.find('table>tfoot>tr');
      expect($trf).to.have.length(1);
    });

    it('В подвале 2 ячейки', () => {
      let $tdf = $section.find('table>tfoot>tr>td');
      expect($tdf).to.have.length(2);
    });

    // Сравнивает нормально, но если студент введет цифру в другом формате (без пробела/без руб.) - выдаст ошибку
    it('Текст в tfoot совпадает', () => {
      let content = $section.find('table>tfoot>tr>td').map((i, node) => {
        return $(node).text().toLowerCase();
      }).get().slice();

      expect(content).to.members(final);
    });
  });

  describe('Задача №4. Исправлена таблица', () => {

    before (() => {
      $section = $('section').eq(3);
    })

    it('Есть таблица', () => {
      let $table = $section.find('table');
      expect($table).to.have.length(1);
    });

    it('Есть шапка таблицы', () => {
      let $thead = $section.find('table>thead');
      expect($thead).to.have.length(1);
    });

    it('В шапке есть одна ячейка с атрибутом colspan', () => {
      let $colspan = $section.find('table>thead>tr>td[colspan="2"]');
      expect($colspan).to.have.length(1);
    });

    it('В шапке есть две ячейки с атрибутом rowspan', () => {
      let $rowspan = $section.find('table>thead>tr>td[rowspan="2"]');
      expect($rowspan).to.have.length(2);
    });

    it('В шапке 2 ряда и 5 ячеек', () => {
      let $trh = $section.find('table>thead>tr');
      expect($trh).to.have.length(2);

      let $tdh = $section.find('table>thead>tr>td');
      expect($tdh).to.have.length(5);
    });

    it('В теле 5 рядов и 19 ячеек', () => {
      let $trb = $section.find('table>tbody>tr');
      expect($trb).to.have.length(5);

      let $tdb = $section.find('table>tbody>tr>td');
      expect($tdb).to.have.length(19);
    });

    it('В теле есть одна ячейка с атрибутом rowspan', () => {
      let $bRowspan = $section.find('table>tbody>tr>td[rowspan="2"]');
      expect($bRowspan).to.have.length(1);
    });
  });
});
